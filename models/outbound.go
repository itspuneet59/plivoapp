package models

import (
	"encoding/json"
	"net/http"
	"plivoapp/cache"
	"plivoapp/db"
	"strings"
)

type OutboundSMS struct {
	account *Account
	sms     *SMS
}

func NewOutboundSMS(data []byte, account *Account) (*OutputMessage, *OutboundSMS) {
	sms := &SMS{}
	err := json.Unmarshal(data, sms)
	if err != nil {
		return NewOutputMessage(NO_MESSAGE, ERROR_UNKNOWN, http.StatusBadRequest), nil
	}

	return nil, &OutboundSMS{
		sms:     sms,
		account: account,
	}
}

func (outbound *OutboundSMS) Validate() *OutputMessage {
	ok, outputMsg := outbound.sms.validate()
	if !ok {
		return outputMsg
	}

	return nil
}

func (outbound *OutboundSMS) CheckPhoneNo(db db.DBAdaptor) *OutputMessage {
	if !outbound.account.verifyPhoneNo(outbound.sms.getFrom(), db) {
		return NewOutputMessage(NO_MESSAGE, ERROR_FROM_PARAM_NOT_FOUND, http.StatusBadRequest)
	}

	return nil
}
func (outbound *OutboundSMS) CheckForSend(cache cache.CacheAdaptor) *OutputMessage {
	ok, errStr := cache.CanSend(outbound.sms.getFrom(), outbound.sms.getTo())
	if !ok {
		if strings.Contains(errStr, "sms from") || strings.Contains(errStr, "limit reached") {
			return NewOutputMessage(NO_MESSAGE, errStr, http.StatusForbidden)
		} else {
			return NewOutputMessage(NO_MESSAGE, errStr, http.StatusInternalServerError)
		}
	}

	return nil
}
