package models

import (
	"os"
	"plivoapp/config"
	"plivoapp/logger"
)

const (
	test_username = "test"
	test_password = "test"
)

func setUp() (error, *config.Config) {
	os.Setenv("GOENV", "development")
	err, conf := config.Get()
	if err != nil {
		return err, nil
	}

	logger.Init(conf.GetLogLevel())
	return nil, conf
}
