package util

import (
	"fmt"
	"io/ioutil"
	"plivoapp/logger"
)

func ReadScript(filePath string) (error, []byte) {
	fileBytes, err := ioutil.ReadFile(filePath)
	if err != nil {
		logger.LogError(fmt.Sprintf("util: file read err = %s", err.Error()))
		return err, []byte{}
	}

	return nil, fileBytes
}
