package logger

// Logger Names
const (
	ErrorLog  = "errorlog"
	UpdateLog = "updateloging"
)

// Log file paths
const (
	CritcialLogFilePath = "logs/critcial.log"
	ErrorLogFilePath    = "logs/error.log"
	NoticeLogFilePath   = "logs/notice.log"
	AccessLogFilePath   = "logs/access.log"
)
