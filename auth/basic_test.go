package auth

import (
	sqlmock "github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"os"
	"plivoapp/config"
	"plivoapp/db"
	"plivoapp/logger"
	"plivoapp/models"
	"plivoapp/util"
	"testing"
)

const (
	TEST_USERNAME = "test"
	TEST_PASSWORD = "test"
)

type httpRequestResponse struct {
	req      *http.Request
	rec      *httptest.ResponseRecorder
	success  bool
	account  *models.Account
	username string
	password string
	row      *sqlmock.Rows
}

func setUp() (error, *config.Config) {
	os.Setenv("GOENV", "development")
	err, conf := config.Get()
	if err != nil {
		return err, nil
	}

	logger.Init(conf.GetLogLevel())
	return nil, conf
}

func prepareData() []*httpRequestResponse {
	testData := []*httpRequestResponse{
		&httpRequestResponse{
			req:      util.ConvertToList(http.NewRequest("POST", "/inbound/sms", nil))[0].(*http.Request),
			rec:      httptest.NewRecorder(),
			success:  false,
			account:  nil,
			username: "",
			password: "",
			row:      sqlmock.NewRows([]string{"id", "auth_id"}),
		},
		&httpRequestResponse{
			req:      util.ConvertToList(http.NewRequest("POST", "/inbound/sms", nil))[0].(*http.Request),
			rec:      httptest.NewRecorder(),
			success:  false,
			account:  nil,
			username: TEST_USERNAME,
			password: "",
			row:      sqlmock.NewRows([]string{"id", "auth_id"}),
		},
		&httpRequestResponse{
			req:      util.ConvertToList(http.NewRequest("POST", "/inbound/sms", nil))[0].(*http.Request),
			rec:      httptest.NewRecorder(),
			success:  false,
			account:  nil,
			username: "",
			password: TEST_PASSWORD,
			row:      sqlmock.NewRows([]string{"id", "auth_id"}),
		},
		&httpRequestResponse{
			req:      util.ConvertToList(http.NewRequest("POST", "/inbound/sms", nil))[0].(*http.Request),
			rec:      httptest.NewRecorder(),
			success:  false,
			account:  nil,
			username: TEST_USERNAME,
			password: TEST_PASSWORD,
			row:      sqlmock.NewRows([]string{"id", "auth_id"}),
		},
		&httpRequestResponse{
			req:      util.ConvertToList(http.NewRequest("POST", "/inbound/sms", nil))[0].(*http.Request),
			rec:      httptest.NewRecorder(),
			success:  true,
			account:  &models.Account{Id: 1, Username: TEST_USERNAME, Password: TEST_PASSWORD},
			username: TEST_USERNAME,
			password: TEST_PASSWORD,
			row:      sqlmock.NewRows([]string{"id", "auth_id"}).AddRow(1, TEST_PASSWORD),
		},
	}

	return testData
}
func TestAuthenticate(t *testing.T) {
	setUp()

	testData := prepareData()
	basicAuth := NewBasicAuth()
	for _, r := range testData {
		mockDB, mock, _ := sqlmock.New()
		defer mockDB.Close()
		sqlDB := db.NewPGSqlMock(mockDB)
		r.req.SetBasicAuth(r.username, r.password)
		mock.ExpectQuery("SELECT id, auth_id FROM account WHERE username = ?").
			WithArgs(TEST_USERNAME).
			WillReturnRows(r.row)
		success, account := basicAuth.Authenticate(r.req, sqlDB)
		assert.Equal(t, r.success, success)
		if r.account == nil {
			assert.Nil(t, account)
		} else {
			assert.NotNil(t, account)
			assert.Equal(t, r.account.Id, account.Id)
			assert.Equal(t, r.account.Password, account.Password)
		}
	}
}
