package handler

const (
	ERROR_UNKNOWN                = "unknown failure"
	ERROR_NONE                   = ""
	ERROR_AUTHENTICATION_FAILURE = "authentication failure"
	MESSAGE_ON_ERROR             = ""
	MESSAGE_SMS_INBOUND_SUCCESS  = "inbound sms ok"
	MESSAGE_SMS_OUTBOUND_SUCCESS = "outbound sms ok"
	NO_MESSAGE                   = ""
)
