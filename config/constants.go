package config

// Messages
const (
	GO_ENV_NOT_SET    = "GO Env not set"
	ERROR_CONFIG_FILE = "Error in config file"
)
