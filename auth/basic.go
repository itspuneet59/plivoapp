package auth

import (
	"fmt"
	"net/http"
	"plivoapp/db"
	"plivoapp/logger"
	"plivoapp/models"
)

type BasicAuth struct {
}

/**
 * Builds an BasicAuth struct
 * @param username string
 * @param password string
 * @return *BasicAuth
 */
func NewBasicAuth() *BasicAuth {
	return &BasicAuth{}
}

/**
 * Authenticates an account
 * @param models.Account account
 * @return boolean indicating if the account got authenticated or not
 */
func (auth *BasicAuth) Authenticate(req *http.Request, db db.DBAdaptor) (bool, *models.Account) {
	reqUserName, reqPassword, authOk := req.BasicAuth()

	// null strings for username and password are not allowed
	if !authOk || len(reqUserName) == 0 || len(reqPassword) == 0 {
		logger.LogError(fmt.Sprintf("authentication failure: authOk = %v, reqUserName = %s, reqPassword = %s", authOk, reqUserName, reqPassword))
		return false, nil
	}

	err, account := models.GetAccount(reqUserName, db)
	if err != nil {
		logger.LogError(fmt.Sprintf("authentication failure: error = %s", err.Error()))
		return false, nil
	}

	// authId validation
	if reqPassword != account.GetPassword() {
		logger.LogError(fmt.Sprintf("authentication failure: password mismatch, reqPassword = %s, account password = %s", reqPassword, account.GetPassword()))
		return false, nil
	}

	return true, account
}
