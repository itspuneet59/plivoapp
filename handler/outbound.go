package handler

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"plivoapp/logger"
	"plivoapp/middleware"
	"plivoapp/models"
)

func HandleOutboundSMS(context *middleware.HandlerContext) *models.OutputMessage {
	body, err := ioutil.ReadAll(context.Request.Body)
	if err != nil || len(body) == 0 {
		return models.NewOutputMessage(NO_MESSAGE, ERROR_UNKNOWN, http.StatusBadRequest)
	}

	logger.LogAccess(fmt.Sprintf("Request: body = %s", body))

	ok, account := context.Env.Auth.Authenticate(context.Request, context.Env.DB)
	if !ok {
		return models.NewOutputMessage(NO_MESSAGE, ERROR_AUTHENTICATION_FAILURE, http.StatusForbidden)
	}

	outputMsg, outBoundSMS := models.NewOutboundSMS([]byte(body), account)
	if outputMsg != nil {
		return outputMsg
	}

	outputMsg = outBoundSMS.Validate()
	if outputMsg != nil {
		return outputMsg
	}

	outputMsg = outBoundSMS.CheckPhoneNo(context.Env.DB)
	if outputMsg != nil {
		return outputMsg
	}

	outputMsg = outBoundSMS.CheckForSend(context.Env.Cache)
	if outputMsg != nil {
		return outputMsg
	}

	return models.NewOutputMessage(MESSAGE_SMS_OUTBOUND_SUCCESS, ERROR_NONE, http.StatusOK)
}
