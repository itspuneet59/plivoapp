package models

import (
	"database/sql"
	"fmt"
	"plivoapp/db"
	"plivoapp/logger"
)

const (
	TABLE_NAME_ACCOUNTS        = "account"
	ACCOUNT_COLUMN_ID          = "id"
	ACCOUNT_COLUMN_USERNAME    = "username"
	ACCOUNT_COLUMN_AUTHID      = "auth_id"
	TABLE_NAME_PHONE_NO        = "phone_number"
	PHONE_NO_COLUMN_ID         = "id"
	PHONE_NO_COLUMN_NUMBER     = "number"
	PHONE_NO_COLUMN_ACCOUNT_ID = "account_id"
)

type Account struct {
	Id       int
	Username string
	Password string
}

func (a *Account) GetId() int {
	return a.Id
}

func (a *Account) GetUsername(username string) string {
	return a.Username
}

func (a *Account) GetPassword() string {
	return a.Password
}

func GetAccount(username string, db db.DBAdaptor) (error, *Account) {
	query := fmt.Sprintf("SELECT %s, %s FROM %s WHERE %s = $1",
		ACCOUNT_COLUMN_ID,
		ACCOUNT_COLUMN_AUTHID,
		TABLE_NAME_ACCOUNTS,
		ACCOUNT_COLUMN_USERNAME)

	account := &Account{}
	err := db.QueryRow(query, username).Scan(&account.Id, &account.Password)
	if err != nil {
		return err, nil
	}

	account.Username = username
	return nil, account
}

func (a *Account) verifyPhoneNo(phoneNo string, db db.DBAdaptor) bool {
	query := fmt.Sprintf("SELECT %s FROM %s WHERE %s = $1 AND %s = $2",
		PHONE_NO_COLUMN_ID, TABLE_NAME_PHONE_NO, PHONE_NO_COLUMN_ACCOUNT_ID, PHONE_NO_COLUMN_NUMBER)

	var id int
	err := db.QueryRow(query, a.Id, phoneNo).Scan(&id)
	if err == sql.ErrNoRows {
		logger.LogError(fmt.Sprintf("db: No row found for phone no = %s", phoneNo))
		return false
	} else if err != nil {
		logger.LogError(fmt.Sprintf("db: err fetching row for phone num = %s", phoneNo))
		return false
	}

	return true
}
