package auth

import (
	"errors"
	"net/http"
	"plivoapp/db"
	"plivoapp/models"
)

type AuthInterface interface {
	Authenticate(req *http.Request, db db.DBAdaptor) (bool, *models.Account)
}

func GetAuthInterface(authType string) (error, AuthInterface) {
	switch authType {
	case AUTH_TYPE_BASIC:
		return nil, NewBasicAuth()
	default:
		return errors.New(AUTH_TYPE_NOT_SET), nil
	}
}
