- For setup run this script
sudo apt-get -y install postgresql
sudo apt-get -y install redis-server
sudo apt-get -y install golang
https://gist.githubusercontent.com/kumaresan-plivo/928368f0cd97b0a35c79d91948491747/raw/954ef7f33226bba969720c53789
2e7464bc7d8ec/testdatadump.sql
mkdir -p ~/go/src/
cd ~/go/src/
git clone https://itspuneet59@bitbucket.org/itspuneet59/plivoapp.git
echo 'export GOENV=production' >> ~/.bashrc
echo 'export GOROOT=/usr/lib/go-1.6/'  >> ~/.bashrc
echo 'export GOPATH=$HOME/go'  >> ~/.bashrc
echo 'export PATH=$PATH:$GOROOT/bin'  >> ~/.bashrc
source ~/.bashrc
cd plivoapp
go get ./...
cd ../
git clone https://itspuneet59@bitbucket.org/itspuneet59/plivointegtest.git
cd plivointegtest
go get ./...
cd ../
sudo redis-server ~/go/src/plivointegtest/redis.conf


- For DB setup
 Postgres got installed with setup script run. Now need to setup password and create the db’s
 sudo passwd postgres,  enter the password - I have kept it Abcd@1234. If you don’t keep it the same, you would need to change plivoapp/config/config.yaml
 sudo su - password
 psql
 On postgres prompt use this command, \password postgres, again keep the password as Abcd@1234
 CREATE DATABASE plivotestdb;
 CREATE DATABASE plivointegdb;
 Verify if the databases are created with \l on Postgres shell
 psql plivotestdb < ~/testdatadump.sql (replace this filepath)
 psql plivointegdb < ~/testdatadump.sql  (replace this filepath)


- If needed please open port 8080(for application) and 8081(for integration testing)
    To Run the application:
    cd ~/go/src/plivoapp
    go build -o plivoapp
    ./plivoapp
    nohup ./plivoapp & (nohup version)

- Run integration tests  
    cd ~/go/src/plivointegtest/
    ./runtest.sh
