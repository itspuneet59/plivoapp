package models

import (
	sqlmock "github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
	"net/http"
	"plivoapp/cache"
	"plivoapp/db"
	"testing"
)

type outboundSMSvalidateReqResponse struct {
	data       string
	success    bool
	outMessage *OutputMessage
}

func TestNewOutboundSMSFailure(t *testing.T) {
	setUp()
	str := "{\"to\":\"xyz\", \"from\":\"pqr\", \"text\":123}"
	outMessage, sms := NewOutboundSMS([]byte(str), nil)
	assert.Nil(t, sms)
	assert.Equal(t, outMessage.Message, NO_MESSAGE)
	assert.Equal(t, outMessage.Error, ERROR_UNKNOWN)
	assert.Equal(t, outMessage.StatusCode, http.StatusBadRequest)
}

func prepareOutboundTestData() []*outboundSMSvalidateReqResponse {
	testData := []*outboundSMSvalidateReqResponse{
		&outboundSMSvalidateReqResponse{
			data:       "{\"to\":\"\", \"from\":\"pqr\",\"text\":\"123\"}",
			success:    false,
			outMessage: NewOutputMessage("", "to is missing", http.StatusBadRequest),
		},
		&outboundSMSvalidateReqResponse{
			data:       "{\"to\":\"xyz\", \"from\":\"\",\"text\":\"123\"}",
			success:    false,
			outMessage: NewOutputMessage("", "from is missing", http.StatusBadRequest),
		},
		&outboundSMSvalidateReqResponse{
			data:       "{\"to\":\"xyz\", \"from\":\"pqr\",\"text\":\"\"}",
			success:    false,
			outMessage: NewOutputMessage("", "text is missing", http.StatusBadRequest),
		},
		&outboundSMSvalidateReqResponse{
			data:       "{\"to\":\"xyz\", \"from\":\"pqr\",\"text\":\"123\"}",
			success:    true,
			outMessage: nil,
		},
	}

	return testData
}

func TestNewOutboundSMS(t *testing.T) {
	setUp()
	str := "{\"to\":\"xyz\", \"from\":\"pqr\", \"text\":\"123\"}"
	outMessage, sms := NewOutboundSMS([]byte(str), nil)
	assert.Nil(t, outMessage)
	assert.NotNil(t, sms)
}

func TestOutboundSMSValidate(t *testing.T) {
	testData := prepareData()
	for _, r := range testData {
		outMessage, outboundSms := NewOutboundSMS([]byte(r.data), nil)
		assert.Nil(t, outMessage)
		assert.NotNil(t, outboundSms)
		outMessage = outboundSms.Validate()
		if r.outMessage != nil {
			assert.Equal(t, outMessage.Message, r.outMessage.Message)
			assert.Equal(t, outMessage.Error, r.outMessage.Error)
			assert.Equal(t, outMessage.StatusCode, r.outMessage.StatusCode)
		} else {
			assert.Nil(t, outMessage)
		}
	}
}

func TestOutboundSMSCheckPhoneNoFailure(t *testing.T) {
	mockDB, mock, _ := sqlmock.New()
	defer mockDB.Close()
	sqlDB := db.NewPGSqlMock(mockDB)
	account := &Account{
		Id:       1,
		Username: "xyz",
		Password: "test",
	}
	str := "{\"to\":\"xyz\", \"from\":\"pqr\", \"text\":\"123\"}"
	_, outboundSms := NewOutboundSMS([]byte(str), account)
	mock.ExpectQuery("SELECT id FROM phone_number WHERE .*").WithArgs(1, "xyz")
	outMessage := outboundSms.CheckPhoneNo(sqlDB)
	assert.Equal(t, "", outMessage.Message)
	assert.Equal(t, "from parameter not found", outMessage.Error)
	assert.Equal(t, http.StatusBadRequest, outMessage.StatusCode)
}

func TestOutboundSMSCheckPhoneNoSuccess(t *testing.T) {
	mockDB, mock, _ := sqlmock.New()
	defer mockDB.Close()
	sqlDB := db.NewPGSqlMock(mockDB)
	account := &Account{
		Id:       1,
		Username: "xyz",
		Password: "test",
	}
	str := "{\"to\":\"pqr\", \"from\":\"xyz\", \"text\":\"123\"}"
	_, outboundSms := NewOutboundSMS([]byte(str), account)
	mock.ExpectQuery("SELECT id FROM phone_number WHERE .*").WithArgs(1, "xyz").WillReturnRows(sqlmock.NewRows([]string{"id"}).AddRow(1))
	outMessage := outboundSms.CheckPhoneNo(sqlDB)
	assert.Nil(t, outMessage)
}

func TestOutboundSMSCheckForSendFail(t *testing.T) {
	account := &Account{
		Id:       1,
		Username: "xyz",
		Password: "test",
	}

	cache := &cache.RedisCacheMock{}
	cache.Success = false
	cache.Error = "unknown failure"

	str := "{\"to\":\"pqr\", \"from\":\"xyz\", \"text\":\"123\"}"
	_, outboundSms := NewOutboundSMS([]byte(str), account)
	outMsg := outboundSms.CheckForSend(cache)
	assert.NotNil(t, outMsg)
	assert.Equal(t, outMsg.Message, "")
	assert.Equal(t, outMsg.Error, "unknown failure")
	assert.Equal(t, outMsg.StatusCode, http.StatusInternalServerError)
}

func TestOutboundSMSCheckForSendSuccess(t *testing.T) {
	account := &Account{
		Id:       1,
		Username: "xyz",
		Password: "test",
	}

	cache := &cache.RedisCacheMock{}
	str := "{\"to\":\"pqr\", \"from\":\"xyz\", \"text\":\"1223\"}"
	_, outboundSms := NewOutboundSMS([]byte(str), account)
	cache.Success = true
	cache.Error = ""
	outMsg := outboundSms.CheckForSend(cache)
	assert.Nil(t, outMsg)
}
