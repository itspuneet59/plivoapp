package models

const (
	ERROR_UNKNOWN                = "unknown failure"
	ERROR_REQUIRED_PARAM_MISSING = "%s is missing"
	ERROR_REQUIRED_PARAM_INVALID = "%s is invalid"
	ERROR_TO_PARAM_NOT_FOUND     = "to parameter not found"
	ERROR_FROM_PARAM_NOT_FOUND   = "from parameter not found"
	NO_MESSAGE                   = ""
)
