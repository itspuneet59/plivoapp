package middleware

import (
	"encoding/json"
	"fmt"
	"net/http"
	"plivoapp/env"
	"plivoapp/logger"
	"plivoapp/models"
)

type HandlerContext struct {
	Env      *env.Environment
	Request  *http.Request
	Response http.ResponseWriter
	Account  *models.Account
}

type HandlerContextFunc func(*HandlerContext) *models.OutputMessage

func handleResponse(handlerFunc HandlerContextFunc) HandlerContextFunc {
	return func(context *HandlerContext) *models.OutputMessage {
		outputMessage := handlerFunc(context)
		context.Response.WriteHeader(outputMessage.StatusCode)
		outputMessageBytes, _ := json.Marshal(outputMessage)
		logger.LogAccess(fmt.Sprintf("Response: status = %d, payload = %s", outputMessage.StatusCode, string(outputMessageBytes)))
		fmt.Fprintf(context.Response, string(outputMessageBytes))
		return nil
	}
}

func handlePanic(handlerFunc HandlerContextFunc) HandlerContextFunc {
	return func(context *HandlerContext) *models.OutputMessage {
		defer func() {
			if err := recover(); err != nil {
				// errString := fmt.Sprintf("%v", err)
				// logger.LogCritical(utils.Concat("Panic occurred : ", errString))
			}
		}()
		return handlerFunc(context)
	}
}

func Execute(env *env.Environment, handlerFunc HandlerContextFunc) func(http.ResponseWriter, *http.Request) {
	handlerFunc = handleResponse(handlerFunc)
	handlerFunc = handlePanic(handlerFunc)

	return func(w http.ResponseWriter, r *http.Request) {
		handlerFunc(&HandlerContext{
			Env:      env,
			Request:  r,
			Response: w,
			Account:  nil,
		})
	}
}
