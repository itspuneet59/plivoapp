package models

import (
	"encoding/json"
	"fmt"
	"net/http"
	"plivoapp/cache"
	"plivoapp/db"
	"plivoapp/logger"
)

type InboundSMS struct {
	account *Account
	sms     *SMS
}

func NewInboundSMS(data []byte, account *Account) (*OutputMessage, *InboundSMS) {
	sms := &SMS{}
	err := json.Unmarshal(data, sms)
	if err != nil {
		logger.LogError(fmt.Sprintf("sms: err = %s", err.Error()))
		return NewOutputMessage(NO_MESSAGE, ERROR_UNKNOWN, http.StatusBadRequest), nil
	}

	return nil, &InboundSMS{
		sms:     sms,
		account: account,
	}
}

func (inbound *InboundSMS) Validate() *OutputMessage {
	ok, outputMsg := inbound.sms.validate()
	if !ok {
		return outputMsg
	}

	return nil
}

func (inbound *InboundSMS) CheckPhoneNo(db db.DBAdaptor) *OutputMessage {
	if !inbound.account.verifyPhoneNo(inbound.sms.getTo(), db) {
		return NewOutputMessage(NO_MESSAGE, ERROR_TO_PARAM_NOT_FOUND, http.StatusBadRequest)
	}

	return nil
}

func (inbound *InboundSMS) UpdateForStop(cache cache.CacheAdaptor) *OutputMessage {
	msg := inbound.sms.getMessage()
	if msg == "STOP" || msg == "STOP\r" || msg == "STOP\n" || msg == "STOP\r\n" {
		logger.LogNotice(fmt.Sprintf("STOP request: from: %s to: %s", inbound.sms.getFrom(), inbound.sms.getTo()))
		ok, errStr := cache.MarkBlocked(inbound.sms.getFrom(), inbound.sms.getTo())
		if !ok {
			logger.LogError(fmt.Sprintf("STOP request: from: %s to: %s STOP failed", inbound.sms.getFrom(), inbound.sms.getTo()))
			return NewOutputMessage(NO_MESSAGE, errStr, http.StatusInternalServerError)
		}
	}

	return nil
}
