package handler

import (
	"bytes"
	sqlmock "github.com/DATA-DOG/go-sqlmock"
	"net/http"
	"net/http/httptest"
	"os"
	"plivoapp/auth"
	"plivoapp/cache"
	"plivoapp/config"
	"plivoapp/db"
	"plivoapp/env"
	"plivoapp/logger"
	"plivoapp/middleware"
	"plivoapp/util"
)

const (
	test_username = "test"
	test_password = "test"
)

func setUp() (error, *config.Config) {
	os.Setenv("GOENV", "development")
	err, conf := config.Get()
	if err != nil {
		return err, nil
	}

	logger.Init(conf.GetLogLevel())
	return nil, conf
}

func setUpHandlerContext(body string, cacheAdaptor cache.CacheAdaptor) (*middleware.HandlerContext, sqlmock.Sqlmock) {
	_, conf := setUp()

	mockDB, mock, _ := sqlmock.New()
	sqlDB := db.NewPGSqlMock(mockDB)

	_, authInterface := auth.GetAuthInterface(conf.GetAuthType())

	env := &env.Environment{
		Auth:  authInterface,
		DB:    sqlDB,
		Cache: cacheAdaptor,
	}

	handlerCtxt := &middleware.HandlerContext{
		Env:      env,
		Request:  util.ConvertToList(http.NewRequest("POST", "/outbound/sms", bytes.NewBufferString(body)))[0].(*http.Request),
		Response: httptest.NewRecorder(),
	}

	return handlerCtxt, mock
}
