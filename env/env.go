package env

import (
	"plivoapp/auth"
	"plivoapp/cache"
	"plivoapp/config"
	"plivoapp/db"
)

type Environment struct {
	Auth  auth.AuthInterface
	DB    db.DBAdaptor
	Cache cache.CacheAdaptor
}

func Initialize(conf *config.Config) *Environment {
	err, db := db.GetDBAdaptor(conf.GetDBType(), conf.GetDBHost(), conf.GetDBPort(), conf.GetDBUserName(), conf.GetDBPassword(), conf.GetDBDatabase())
	if err != nil {
		panic(err)
	}

	err, cache := cache.GetCacheAdaptor(conf.GetCacheType(), conf.GetCacheHost(), conf.GetCachePort())
	if err != nil {
		panic(err)
	}

	err, authInterface := auth.GetAuthInterface(conf.GetAuthType())
	if err != nil {
		panic(err)
	}

	env := &Environment{
		Auth:  authInterface,
		DB:    db,
		Cache: cache,
	}

	return env
}
