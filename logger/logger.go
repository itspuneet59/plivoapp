package logger

import (
	"encoding/json"
	"path"
	"path/filepath"
	"runtime"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/logs"
)

var errorLogger *logs.BeeLogger
var criticalLogger *logs.BeeLogger
var noticeLogger *logs.BeeLogger
var accessLogger *logs.BeeLogger

const loggerFormatString = "PlivoApp:%s"

// InitializeLogger  initialize logger
func InitializeLogger(filePath string, logLevel int) *logs.BeeLogger {
	config := make(map[string]interface{})
	// maxlines default 1000000
	// maxsize default 1 << 28 or 256M
	// daily default true
	// rotate default true
	_, currentFilePath, _, _ := runtime.Caller(0)
	currentAbsDirPath := path.Dir(currentFilePath)
	projectAbsPath := filepath.Join(currentAbsDirPath, "..")
	config["filename"] = filepath.Join(projectAbsPath, filePath)
	config["maxdays"] = 15
	config["level"] = logLevel
	configBytes, err := json.Marshal(config)
	if err != nil {
		panic(err)
	}
	logger := logs.NewLogger()
	err = logger.SetLogger("file", string(configBytes))
	if err != nil {
		panic(err)
	}
	logger.EnableFuncCallDepth(true)
	logger.SetLogFuncCallDepth(3)
	return logger
}

// Init initilize loggers
func Init(logLevel int) {
	beego.SetLevel(logLevel)
	beego.BeeLogger.EnableFuncCallDepth(true)

	errorLogger = InitializeLogger(ErrorLogFilePath, logLevel)
	criticalLogger = InitializeLogger(CritcialLogFilePath, logLevel)
	noticeLogger = InitializeLogger(NoticeLogFilePath, logLevel)
	accessLogger = InitializeLogger(AccessLogFilePath, logLevel)
}

// LogCritical logs critical error
func LogCritical(errorString string) {
	criticalLogger.Critical(loggerFormatString, errorString)
}

// LogError logs error
func LogError(errorString string) {
	errorLogger.Error(loggerFormatString, errorString)
}

// LogNotice logs notice
func LogNotice(errorString string) {
	noticeLogger.Notice(loggerFormatString, errorString)
}

func LogAccess(logString string) {
	accessLogger.Critical(loggerFormatString, logString)
}
