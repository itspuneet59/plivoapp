package config

import (
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"plivoapp/logger"
	"runtime"

	"gopkg.in/yaml.v2"
)

// Manager object will contain configs for all environments
type Manager struct {
	Configs map[string]*Config
}

// Config is per environment config
type Config struct {
	Cache    *Cache
	DB       *DB
	AuthType string
	LogLevel int
	Port     int
}

// AS struct is about aeropsike config
type Cache struct {
	Host string
	Port int
	Type string
}

// DB struct is about sql style database config
type DB struct {
	Host     string
	Port     int
	UserName string
	Password string
	Database string
	Type     string
}

// Get initlilizes config if not initilized already and return
func Get() (error, *Config) {
	//if os.Getenv("GOPATH") == "" {
	//	return nil, errors.New(GO_PATH_NOT_SET)
	//}

	if os.Getenv("GOENV") == "" {
		logger.LogCritical(fmt.Sprintf("config error: %s", GO_ENV_NOT_SET))
		return errors.New(GO_ENV_NOT_SET), nil
	}

	confManager := new(Manager)
	_, currentFilePath, _, _ := runtime.Caller(0)
	yamlData, err := ioutil.ReadFile(path.Join(path.Dir(currentFilePath), "config.yaml"))
	if err != nil {
		logger.LogCritical(fmt.Sprintf("config error: error = %s", err.Error()))
		return err, nil
	}

	err = yaml.Unmarshal([]byte(yamlData), confManager)
	if err != nil {
		logger.LogCritical(fmt.Sprintf("config error: error = %s", err.Error()))
		return err, nil
	}

	conf := confManager.Configs[os.Getenv("GOENV")]
	if conf == nil {
		logger.LogCritical(fmt.Sprintf("config error: error = %s", ERROR_CONFIG_FILE))
		return errors.New(ERROR_CONFIG_FILE), nil
	}

	return nil, conf
}

// GetCacheHost gets cache host from the given config
func (c *Config) GetCacheHost() string {
	return c.Cache.Host
}

// GetCachePort gets cache port from the given config
func (c *Config) GetCachePort() int {
	return c.Cache.Port
}

// GetCacheType gets cache type
func (c *Config) GetCacheType() string {
	return c.Cache.Type
}

// GetDBHost gets sql host
func (c *Config) GetDBHost() string {
	return c.DB.Host
}

// GetDBPort gets sql port
func (c *Config) GetDBPort() int {
	return c.DB.Port
}

// GetDBUserName gets sql user name
func (c *Config) GetDBUserName() string {
	return c.DB.UserName
}

// GetDBPassword gets sql password
func (c *Config) GetDBPassword() string {
	return c.DB.Password
}

// GetDBDatabase gets sql database name
func (c *Config) GetDBDatabase() string {
	return c.DB.Database
}

// GetDBType gets sql database type
func (c *Config) GetDBType() string {
	return c.DB.Type
}

// GetLogLevel gets log level
func (c *Config) GetLogLevel() int {
	return c.LogLevel
}

// GetAuthType gets authentication type
func (c *Config) GetAuthType() string {
	return c.AuthType
}

func (c *Config) GetPort() int {
	return c.Port
}
