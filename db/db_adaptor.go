package db

import (
	"database/sql"
	"errors"
)

const (
	DB_MYSQL = "mysql"
	DB_PGSQL = "pgsql"
)

type DBAdaptor interface {
	QueryRow(string, ...interface{}) *sql.Row
	Close()
}

func GetDBAdaptor(dbType string,
	host string, port int, username string,
	password string, db string) (error, DBAdaptor) {
	switch dbType {
	case DB_PGSQL:
		return NewPGSql(host, port, username, password, db)
	default:
		return errors.New("DBTYPE not implemented"), nil

	}
}
