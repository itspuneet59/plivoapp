package cache

import ()

type RedisCacheMock struct {
	Success bool
	Error   string
}

func (rc *RedisCacheMock) CanSend(from string, to string) (bool, string) {
	return rc.Success, rc.Error
}

func (rc *RedisCacheMock) MarkBlocked(from string, to string) (bool, string) {
	return rc.Success, rc.Error
}
