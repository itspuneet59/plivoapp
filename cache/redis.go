package cache

import (
	"fmt"
	pool "github.com/mediocregopher/radix.v2/pool"
	radixUtil "github.com/mediocregopher/radix.v2/util"
	"plivoapp/logger"
	"plivoapp/util"
)

const (
	REDIS_ERROR_UNKNOWN       = -1
	REDIS_ERROR_LIMIT_REACHED = 1
	REDIS_ERROR_SEND_BLOCKED  = 2
	REDIS_SUCCESS             = 0
	COUNT_KEY_PREFIX          = "count:"
	STATUS_KEY_PREFIX         = "status:"
	LIMIT_PER_PHONE_NUMBER    = 50
	STATUS_KEY_VALUE          = "blocked"
	/* 4 hours */
	STATUS_KEY_EXPIRY = 14400
	/* 24 hours */
	COUNT_KEY_EXPIRY    = 86400
	ERROR_UNKNOWN       = "unknown failure"
	ERROR_SMS_BLOCKED   = "sms from %s to %s blocked by STOP request"
	ERROR_LIMIT_REACHED = "limit reached for from %s"
)

type RedisCache struct {
	pool          *pool.Pool
	canSendScript string
}

const (
	NUM_CONNECTIONS = 50
	SOCKET_TCP      = "tcp"
)

func NewRedis(host string, port int) (error, *RedisCache) {
	p, err := pool.New(SOCKET_TCP, fmt.Sprintf("%s:%d", host, port), NUM_CONNECTIONS)
	if err != nil {
		return err, nil
	}

	err, scriptContent := util.ReadScript("./cache/can_send.lua")
	if err != nil {
		return err, nil
	}

	return nil, &RedisCache{
		pool:          p,
		canSendScript: string(scriptContent),
	}
}

func (rc *RedisCache) CanSend(from string, to string) (bool, string) {
	conn, err := rc.pool.Get()
	if err != nil {
		return false, ERROR_UNKNOWN
	}

	defer rc.pool.Put(conn)
	countKey := fmt.Sprintf("%s%s", COUNT_KEY_PREFIX, from)
	statusKey := fmt.Sprintf("%s%s_%s", STATUS_KEY_PREFIX, to, from)
	redisErr := radixUtil.LuaEval(conn, rc.canSendScript, 2, countKey, statusKey,
		LIMIT_PER_PHONE_NUMBER, COUNT_KEY_EXPIRY, STATUS_KEY_VALUE)

	ret, _ := redisErr.Int()
	switch ret {
	case -1:
		return false, ERROR_UNKNOWN
	case 1:
		return false, fmt.Sprintf(ERROR_LIMIT_REACHED, from)
	case 2:
		return false, fmt.Sprintf(ERROR_SMS_BLOCKED, from, to)
	case 0:
		return true, ""
	default:
		logger.LogError(fmt.Sprintf("Unexpected return value = %v", ret))
		return false, ""
	}
}

func (rc *RedisCache) MarkBlocked(from string, to string) (bool, string) {
	conn, err := rc.pool.Get()
	if err != nil {
		logger.LogCritical(fmt.Sprintf("redis error: connection not available in pool"))
		return false, ERROR_UNKNOWN
	}

	defer rc.pool.Put(conn)
	redisErr := conn.Cmd("SETEX", fmt.Sprintf("%s%s_%s", STATUS_KEY_PREFIX, from, to),
		STATUS_KEY_EXPIRY, STATUS_KEY_VALUE).Err
	if redisErr != nil {
		logger.LogError(fmt.Sprintf("redis error: %s", redisErr))
		return false, ERROR_UNKNOWN
	}

	return true, ""
}
