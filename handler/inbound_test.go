package handler

import (
	sqlmock "github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
	"net/http"
	"plivoapp/cache"
	"testing"
)

func TestInboundSMSNilBody(t *testing.T) {
	handlerCnxt, _ := setUpHandlerContext("", &cache.RedisCacheMock{})
	defer handlerCnxt.Env.DB.Close()

	output := HandleInboundSMS(handlerCnxt)
	assert.NotNil(t, output)
	assert.Equal(t, "", output.Message)
	assert.Equal(t, "unknown failure", output.Error)
	assert.Equal(t, http.StatusBadRequest, output.StatusCode)
}

func TestInboundSMSAuthFailure(t *testing.T) {
	cxt, mock := setUpHandlerContext("{\"x\":\"y\"}", &cache.RedisCacheMock{})
	defer cxt.Env.DB.Close()

	cxt.Request.SetBasicAuth("test", "test1")

	mock.ExpectQuery("SELECT id, auth_id FROM account WHERE .*").
		WithArgs("test").
		WillReturnRows(sqlmock.NewRows([]string{"id", "auth_id"}).AddRow(1, "test"))

	output := HandleInboundSMS(cxt)
	assert.NotNil(t, output)
	assert.Equal(t, "", output.Message)
	assert.Equal(t, "authentication failure", output.Error)
	assert.Equal(t, http.StatusForbidden, output.StatusCode)
}

func TestInboundSMSNewInboundSMSFailure(t *testing.T) {
	cxt, mock := setUpHandlerContext("{\"to\":\"xyz\",\"from\":\"pqr\",\"text\":123}", &cache.RedisCacheMock{})
	defer cxt.Env.DB.Close()

	cxt.Request.SetBasicAuth("test", "test")

	mock.ExpectQuery("SELECT id, auth_id FROM account WHERE .*").
		WithArgs("test").
		WillReturnRows(sqlmock.NewRows([]string{"id", "auth_id"}).AddRow(1, "test"))

	output := HandleInboundSMS(cxt)
	assert.NotNil(t, output)
	assert.Equal(t, "", output.Message)
	assert.Equal(t, "unknown failure", output.Error)
	assert.Equal(t, http.StatusBadRequest, output.StatusCode)
}

func TestInboundSMSValidateFailure(t *testing.T) {
	cxt, mock := setUpHandlerContext("{\"to\":\"xyz\",\"from\":\"pqr\",\"text\":\"\"}", &cache.RedisCacheMock{})
	defer cxt.Env.DB.Close()

	cxt.Request.SetBasicAuth("test", "test")

	mock.ExpectQuery("SELECT id, auth_id FROM account WHERE .*").
		WithArgs("test").
		WillReturnRows(sqlmock.NewRows([]string{"id", "auth_id"}).AddRow(1, "test"))

	output := HandleInboundSMS(cxt)
	assert.NotNil(t, output)
	assert.Equal(t, "", output.Message)
	assert.Equal(t, "text is missing", output.Error)
	assert.Equal(t, http.StatusBadRequest, output.StatusCode)
}

func TestInboundSMSCheckPhoneNoFailure(t *testing.T) {
	cxt, mock := setUpHandlerContext("{\"to\":\"xyz\",\"from\":\"pqr\",\"text\":\"hello\"}", &cache.RedisCacheMock{})
	defer cxt.Env.DB.Close()

	cxt.Request.SetBasicAuth("test", "test")

	mock.ExpectQuery("SELECT id, auth_id FROM account WHERE .*").
		WithArgs("test").
		WillReturnRows(sqlmock.NewRows([]string{"id", "auth_id"}).AddRow(1, "test"))

	mock.ExpectQuery("SELECT id FROM phone_number WHERE .*").
		WithArgs("test").
		WillReturnRows(sqlmock.NewRows([]string{"id"}).AddRow(1))

	output := HandleInboundSMS(cxt)
	assert.NotNil(t, output)
	assert.Equal(t, "", output.Message)
	assert.Equal(t, "to parameter not found", output.Error)
	assert.Equal(t, http.StatusBadRequest, output.StatusCode)
}

func TestInboundSMSCheckUpdateForStopFailureWithStopMessage(t *testing.T) {
	cxt, mock := setUpHandlerContext("{\"to\":\"xyz\",\"from\":\"pqr\",\"text\":\"STOP\"}", &cache.RedisCacheMock{Success: false, Error: "Update Error"})
	defer cxt.Env.DB.Close()

	cxt.Request.SetBasicAuth("xyz", "test")

	mock.ExpectQuery("SELECT id, auth_id FROM account WHERE .*").
		WithArgs("xyz").
		WillReturnRows(sqlmock.NewRows([]string{"id", "auth_id"}).AddRow(1, "test"))

	mock.ExpectQuery("SELECT id FROM phone_number WHERE .*").
		WithArgs(1, "xyz").
		WillReturnRows(sqlmock.NewRows([]string{"id"}).AddRow(1))

	output := HandleInboundSMS(cxt)
	assert.NotNil(t, output)
	assert.Equal(t, "", output.Message)
	assert.Equal(t, "Update Error", output.Error)
	assert.Equal(t, http.StatusInternalServerError, output.StatusCode)
}

func TestInboundSMSCheckUpdateForStopSuccessWithStopMessage(t *testing.T) {
	cxt, mock := setUpHandlerContext("{\"to\":\"xyz\",\"from\":\"pqr\",\"text\":\"STOP\"}", &cache.RedisCacheMock{Success: true, Error: ""})
	defer cxt.Env.DB.Close()

	cxt.Request.SetBasicAuth("xyz", "test")

	mock.ExpectQuery("SELECT id, auth_id FROM account WHERE .*").
		WithArgs("xyz").
		WillReturnRows(sqlmock.NewRows([]string{"id", "auth_id"}).AddRow(1, "test"))

	mock.ExpectQuery("SELECT id FROM phone_number WHERE .*").
		WithArgs(1, "xyz").
		WillReturnRows(sqlmock.NewRows([]string{"id"}).AddRow(1))

	output := HandleInboundSMS(cxt)
	assert.NotNil(t, output)
	assert.Equal(t, "inbound sms ok", output.Message)
	assert.Equal(t, "", output.Error)
	assert.Equal(t, http.StatusOK, output.StatusCode)
}

func TestInboundSMSCheckUpdateForStopSuccessWithoutStopMessage(t *testing.T) {
	cxt, mock := setUpHandlerContext("{\"to\":\"xyz\",\"from\":\"pqr\",\"text\":\"Hello\"}", &cache.RedisCacheMock{Success: true, Error: ""})
	defer cxt.Env.DB.Close()

	cxt.Request.SetBasicAuth("xyz", "test")

	mock.ExpectQuery("SELECT id, auth_id FROM account WHERE .*").
		WithArgs("xyz").
		WillReturnRows(sqlmock.NewRows([]string{"id", "auth_id"}).AddRow(1, "test"))

	mock.ExpectQuery("SELECT id FROM phone_number WHERE .*").
		WithArgs(1, "xyz").
		WillReturnRows(sqlmock.NewRows([]string{"id"}).AddRow(1))

	output := HandleInboundSMS(cxt)
	assert.NotNil(t, output)
	assert.Equal(t, "inbound sms ok", output.Message)
	assert.Equal(t, "", output.Error)
	assert.Equal(t, http.StatusOK, output.StatusCode)
}
