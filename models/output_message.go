package models

type OutputMessage struct {
	Message    string `json:"message"`
	Error      string `json:"error"`
	StatusCode int    `json:"-"`
}

func NewOutputMessage(message string, err string, status int) *OutputMessage {
	return &OutputMessage{
		Message:    message,
		Error:      err,
		StatusCode: status,
	}
}
