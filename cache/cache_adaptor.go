package cache

import (
	"errors"
	"fmt"
)

const (
	CACHE_REDIS = "redis"
)

type CacheAdaptor interface {
	CanSend(from string, to string) (bool, string)
	MarkBlocked(from string, to string) (bool, string)
}

func GetCacheAdaptor(cacheType string, host string, port int) (error, CacheAdaptor) {
	switch cacheType {
	case CACHE_REDIS:
		return NewRedis(host, port)
	default:
		return errors.New(fmt.Sprintf("Cache type %s not implemented", cacheType)), nil

	}
}
