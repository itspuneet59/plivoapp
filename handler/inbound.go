package handler

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"plivoapp/logger"
	"plivoapp/middleware"
	"plivoapp/models"
)

func HandleInboundSMS(context *middleware.HandlerContext) *models.OutputMessage {
	body, err := ioutil.ReadAll(context.Request.Body)
	if err != nil || len(body) == 0 {
		return models.NewOutputMessage(NO_MESSAGE, ERROR_UNKNOWN, http.StatusBadRequest)
	}

	logger.LogAccess(fmt.Sprintf("Request: body = %s", body))

	ok, account := context.Env.Auth.Authenticate(context.Request, context.Env.DB)
	if !ok {
		return models.NewOutputMessage(NO_MESSAGE, ERROR_AUTHENTICATION_FAILURE, http.StatusForbidden)
	}

	outputMsg, inBoundSMS := models.NewInboundSMS([]byte(body), account)
	if outputMsg != nil {
		return outputMsg
	}

	outputMsg = inBoundSMS.Validate()
	if outputMsg != nil {
		return outputMsg
	}

	outputMsg = inBoundSMS.CheckPhoneNo(context.Env.DB)
	if outputMsg != nil {
		return outputMsg
	}

	outputMsg = inBoundSMS.UpdateForStop(context.Env.Cache)
	if outputMsg != nil {
		return outputMsg
	}

	return models.NewOutputMessage(MESSAGE_SMS_INBOUND_SUCCESS, ERROR_NONE, http.StatusOK)
}
