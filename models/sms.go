package models

import (
	"fmt"
	"net/http"
)

type SMS struct {
	To   string `json: "to"`
	From string `json: "from"`
	Text string `json: "text"`
}

func (sms *SMS) validate() (bool, *OutputMessage) {
	if len(sms.To) == 0 {
		return false, NewOutputMessage(NO_MESSAGE, fmt.Sprintf(ERROR_REQUIRED_PARAM_MISSING, "to"), http.StatusBadRequest)
	} else if len(sms.From) == 0 {
		return false, NewOutputMessage(NO_MESSAGE, fmt.Sprintf(ERROR_REQUIRED_PARAM_MISSING, "from"), http.StatusBadRequest)
	} else if len(sms.Text) == 0 {
		return false, NewOutputMessage(NO_MESSAGE, fmt.Sprintf(ERROR_REQUIRED_PARAM_MISSING, "text"), http.StatusBadRequest)
	}

	return true, nil
}

func (sms *SMS) getTo() string {
	return sms.To
}

func (sms *SMS) getFrom() string {
	return sms.From
}

func (sms *SMS) getMessage() string {
	return sms.Text
}
