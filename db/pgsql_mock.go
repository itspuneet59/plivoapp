package db

import (
	"database/sql"
)

type PGSqlMock struct {
	db *sql.DB
}

func NewPGSqlMock(db *sql.DB) *PGSqlMock {
	return &PGSqlMock{
		db: db,
	}
}

func (pgsql *PGSqlMock) QueryRow(query string, args ...interface{}) *sql.Row {
	return pgsql.db.QueryRow(query, args...)

}

func (pgsql *PGSqlMock) Close() {
}
