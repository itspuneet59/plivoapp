package models

import (
	sqlmock "github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
	"plivoapp/db"
	"testing"
)

func TestGetAccountFailure(t *testing.T) {
	mockDB, mock, _ := sqlmock.New()
	defer mockDB.Close()
	sqlDB := db.NewPGSqlMock(mockDB)
	mock.ExpectQuery("SELECT id, auth_id FROM account WHERE .*").
		WithArgs(test_username)
	err, account := GetAccount(test_username, sqlDB)
	assert.NotNil(t, err)
	assert.Nil(t, account)
}

func TestGetAccountSuccess(t *testing.T) {
	mockDB, mock, _ := sqlmock.New()
	defer mockDB.Close()
	sqlDB := db.NewPGSqlMock(mockDB)
	mock.ExpectQuery("SELECT id, auth_id FROM account WHERE .*").
		WithArgs(test_username).
		WillReturnRows(sqlmock.NewRows([]string{"id", "auth_id"}).AddRow(1, test_password))

	err, account := GetAccount(test_username, sqlDB)
	assert.Nil(t, err)
	assert.NotNil(t, account)
	assert.Equal(t, account.Id, 1)
	assert.Equal(t, account.Username, test_username)
	assert.Equal(t, account.Password, test_password)
}
