package main

import (
	"fmt"
	"github.com/gorilla/mux"
	"net/http"
	"plivoapp/config"
	"plivoapp/env"
	"plivoapp/handler"
	"plivoapp/logger"
	"plivoapp/middleware"
)

func main() {
	err, conf := config.Get()
	if err != nil {
		panic(err)
	}

	logger.Init(conf.GetLogLevel())

	env := env.Initialize(conf)
	if env == nil {
		panic(err)
	}

	r := mux.NewRouter()
	postRouter := r.Methods("POST").Subrouter()
	postRouter.HandleFunc("/outbound/sms", middleware.Execute(env, handler.HandleOutboundSMS))
	postRouter.HandleFunc("/inbound/sms", middleware.Execute(env, handler.HandleInboundSMS))
	if err := http.ListenAndServe(fmt.Sprintf(":%d", conf.GetPort()), r); err != nil {
		logger.LogCritical(fmt.Sprintf("ListenAndServe error: %s", err.Error()))
		panic(err)
	}
}
