package models

import (
	sqlmock "github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
	"net/http"
	"plivoapp/cache"
	"plivoapp/db"
	"testing"
)

type inboundSMSvalidateReqResponse struct {
	data       string
	success    bool
	outMessage *OutputMessage
}

func TestNewInboundSMSFailure(t *testing.T) {
	setUp()
	str := "{\"to\":\"xyz\", \"from\":\"pqr\", \"text\":123}"
	outMessage, sms := NewInboundSMS([]byte(str), nil)
	assert.Nil(t, sms)
	assert.Equal(t, outMessage.Message, NO_MESSAGE)
	assert.Equal(t, outMessage.Error, ERROR_UNKNOWN)
	assert.Equal(t, outMessage.StatusCode, http.StatusBadRequest)
}

func prepareData() []*inboundSMSvalidateReqResponse {
	testData := []*inboundSMSvalidateReqResponse{
		&inboundSMSvalidateReqResponse{
			data:       "{\"to\":\"\", \"from\":\"pqr\",\"text\":\"123\"}",
			success:    false,
			outMessage: NewOutputMessage("", "to is missing", http.StatusBadRequest),
		},
		&inboundSMSvalidateReqResponse{
			data:       "{\"to\":\"xyz\", \"from\":\"\",\"text\":\"123\"}",
			success:    false,
			outMessage: NewOutputMessage("", "from is missing", http.StatusBadRequest),
		},
		&inboundSMSvalidateReqResponse{
			data:       "{\"to\":\"xyz\", \"from\":\"pqr\",\"text\":\"\"}",
			success:    false,
			outMessage: NewOutputMessage("", "text is missing", http.StatusBadRequest),
		},
		&inboundSMSvalidateReqResponse{
			data:       "{\"to\":\"xyz\", \"from\":\"pqr\",\"text\":\"123\"}",
			success:    true,
			outMessage: nil,
		},
	}

	return testData
}

func TestNewInboundSMS(t *testing.T) {
	setUp()
	str := "{\"to\":\"xyz\", \"from\":\"pqr\", \"text\":\"123\"}"
	outMessage, sms := NewInboundSMS([]byte(str), nil)
	assert.Nil(t, outMessage)
	assert.NotNil(t, sms)
}

func TestValidate(t *testing.T) {
	testData := prepareData()
	for _, r := range testData {
		outMessage, inboundSms := NewInboundSMS([]byte(r.data), nil)
		assert.Nil(t, outMessage)
		assert.NotNil(t, inboundSms)
		outMessage = inboundSms.Validate()
		if r.outMessage != nil {
			assert.Equal(t, outMessage.Message, r.outMessage.Message)
			assert.Equal(t, outMessage.Error, r.outMessage.Error)
			assert.Equal(t, outMessage.StatusCode, r.outMessage.StatusCode)
		} else {
			assert.Nil(t, outMessage)
		}
	}
}

func TestCheckPhoneNoFailure(t *testing.T) {
	mockDB, mock, _ := sqlmock.New()
	defer mockDB.Close()
	sqlDB := db.NewPGSqlMock(mockDB)
	account := &Account{
		Id:       1,
		Username: "xyz",
		Password: "test",
	}
	str := "{\"to\":\"xyz\", \"from\":\"pqr\", \"text\":\"123\"}"
	_, inboundSms := NewInboundSMS([]byte(str), account)
	mock.ExpectQuery("SELECT id FROM phone_number WHERE .*").WithArgs(1, "xyz")
	outMessage := inboundSms.CheckPhoneNo(sqlDB)
	assert.Equal(t, "", outMessage.Message)
	assert.Equal(t, "to parameter not found", outMessage.Error)
	assert.Equal(t, http.StatusBadRequest, outMessage.StatusCode)
}

func TestCheckPhoneNoSuccess(t *testing.T) {
	mockDB, mock, _ := sqlmock.New()
	defer mockDB.Close()
	sqlDB := db.NewPGSqlMock(mockDB)
	account := &Account{
		Id:       1,
		Username: "xyz",
		Password: "test",
	}
	str := "{\"to\":\"xyz\", \"from\":\"pqr\", \"text\":\"123\"}"
	_, inboundSms := NewInboundSMS([]byte(str), account)
	mock.ExpectQuery("SELECT id FROM phone_number WHERE .*").WithArgs(1, "xyz").WillReturnRows(sqlmock.NewRows([]string{"id"}).AddRow(1))
	outMessage := inboundSms.CheckPhoneNo(sqlDB)
	assert.Nil(t, outMessage)
}

func TestUpdateForNotStopMessage(t *testing.T) {
	account := &Account{
		Id:       1,
		Username: "xyz",
		Password: "test",
	}

	cache := &cache.RedisCacheMock{}
	str := "{\"to\":\"xyz\", \"from\":\"pqr\", \"text\":\"123\"}"
	_, inboundSms := NewInboundSMS([]byte(str), account)
	outMsg := inboundSms.UpdateForStop(cache)
	assert.Nil(t, outMsg)
}

func TestUpdateForStopMessageFailure(t *testing.T) {
	account := &Account{
		Id:       1,
		Username: "xyz",
		Password: "test",
	}

	cache := &cache.RedisCacheMock{}
	str := "{\"to\":\"xyz\", \"from\":\"pqr\", \"text\":\"STOP\"}"
	_, inboundSms := NewInboundSMS([]byte(str), account)
	cache.Success = false
	cache.Error = "unknown failure"
	outMsg := inboundSms.UpdateForStop(cache)
	assert.NotNil(t, outMsg)
	assert.Equal(t, outMsg.Message, "")
	assert.Equal(t, outMsg.Error, "unknown failure")
	assert.Equal(t, outMsg.StatusCode, http.StatusInternalServerError)
}

func TestUpdateForStopMessageSuccess(t *testing.T) {
	account := &Account{
		Id:       1,
		Username: "xyz",
		Password: "test",
	}

	cache := &cache.RedisCacheMock{}
	str := "{\"to\":\"xyz\", \"from\":\"pqr\", \"text\":\"STOP\"}"
	_, inboundSms := NewInboundSMS([]byte(str), account)
	cache.Success = true
	cache.Error = ""
	outMsg := inboundSms.UpdateForStop(cache)
	assert.Nil(t, outMsg)
}
