--[[
    Usage: EVAL can_send.lua 2 count:<phone_number> pair:<from>_<to> max_limit expiry
    returns -1 if unknown error
    returns 1 if limit exceeded
    returns 2 if blocked
    returns 0 if not blocked/not limit reached
 ]]--

if #KEYS ~= 2 or #ARGV ~=3 then
    return -1
end

print("keys1 = "..KEYS[1]..", keys2 = "..KEYS[2]..", ARGv1 = "..ARGV[1]..", arg2 = "..ARGV[2]..", arg3="..ARGV[3])
-- Check for limit exceeded
local sent_count = redis.call("GET", KEYS[1])
-- redis nil converts to boolean in lua
if sent_count ~= false and tonumber(sent_count) >= tonumber(ARGV[1]) then
    return 1 
end

-- Check if the from->to pair is blocked
local status = redis.call("GET", KEYS[2])
if status ~= false and status == ARGV[3] then
    return 2 
end

--[[ If counter does not exist, set the value to 1 with expiry otherwise just
    increment the counter. Key would have the expiry on the time it was first
created ]]--
if sent_count == false then
    redis.call("SETEX", KEYS[1], ARGV[2], 1)
else 
    redis.call("INCR", KEYS[1])
end

return 0 
