package handler

import (
	sqlmock "github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
	"net/http"
	"plivoapp/cache"
	"testing"
)

func TestOutboundSMSNilBody(t *testing.T) {
	handlerCnxt, _ := setUpHandlerContext("", &cache.RedisCacheMock{})
	defer handlerCnxt.Env.DB.Close()

	output := HandleOutboundSMS(handlerCnxt)
	assert.NotNil(t, output)
	assert.Equal(t, "", output.Message)
	assert.Equal(t, "unknown failure", output.Error)
	assert.Equal(t, http.StatusBadRequest, output.StatusCode)
}

func TestOutboundSMSAuthFailure(t *testing.T) {
	cxt, mock := setUpHandlerContext("{\"x\":\"y\"}", &cache.RedisCacheMock{})
	defer cxt.Env.DB.Close()

	cxt.Request.SetBasicAuth("test", "test1")

	mock.ExpectQuery("SELECT id, auth_id FROM account WHERE .*").
		WithArgs("test").
		WillReturnRows(sqlmock.NewRows([]string{"id", "auth_id"}).AddRow(1, "test"))

	output := HandleOutboundSMS(cxt)
	assert.NotNil(t, output)
	assert.Equal(t, "", output.Message)
	assert.Equal(t, "authentication failure", output.Error)
	assert.Equal(t, http.StatusForbidden, output.StatusCode)
}

func TestOutboundSMSNewOutboundSMSFailure(t *testing.T) {
	cxt, mock := setUpHandlerContext("{\"to\":\"xyz\",\"from\":\"pqr\",\"text\":123}", &cache.RedisCacheMock{})
	defer cxt.Env.DB.Close()

	cxt.Request.SetBasicAuth("test", "test")

	mock.ExpectQuery("SELECT id, auth_id FROM account WHERE .*").
		WithArgs("test").
		WillReturnRows(sqlmock.NewRows([]string{"id", "auth_id"}).AddRow(1, "test"))

	output := HandleOutboundSMS(cxt)
	assert.NotNil(t, output)
	assert.Equal(t, "", output.Message)
	assert.Equal(t, "unknown failure", output.Error)
	assert.Equal(t, http.StatusBadRequest, output.StatusCode)
}

func TestOutboundSMSValidateFailure(t *testing.T) {
	cxt, mock := setUpHandlerContext("{\"to\":\"xyz\",\"from\":\"pqr\",\"text\":\"\"}", &cache.RedisCacheMock{})
	defer cxt.Env.DB.Close()

	cxt.Request.SetBasicAuth("test", "test")

	mock.ExpectQuery("SELECT id, auth_id FROM account WHERE .*").
		WithArgs("test").
		WillReturnRows(sqlmock.NewRows([]string{"id", "auth_id"}).AddRow(1, "test"))

	output := HandleOutboundSMS(cxt)
	assert.NotNil(t, output)
	assert.Equal(t, "", output.Message)
	assert.Equal(t, "text is missing", output.Error)
	assert.Equal(t, http.StatusBadRequest, output.StatusCode)
}

func TestOutboundSMSCheckPhoneNoFailure(t *testing.T) {
	cxt, mock := setUpHandlerContext("{\"to\":\"pqr\",\"from\":\"xyz\",\"text\":\"hello\"}", &cache.RedisCacheMock{})
	defer cxt.Env.DB.Close()

	cxt.Request.SetBasicAuth("test", "test")

	mock.ExpectQuery("SELECT id, auth_id FROM account WHERE .*").
		WithArgs("test").
		WillReturnRows(sqlmock.NewRows([]string{"id", "auth_id"}).AddRow(1, "test"))

	mock.ExpectQuery("SELECT id FROM phone_number WHERE .*").
		WithArgs(1, "test").
		WillReturnRows(sqlmock.NewRows([]string{"id"}).AddRow(1))

	output := HandleOutboundSMS(cxt)
	assert.NotNil(t, output)
	assert.Equal(t, "", output.Message)
	assert.Equal(t, "from parameter not found", output.Error)
	assert.Equal(t, http.StatusBadRequest, output.StatusCode)
}

func TestOutboundSMSCheckForSendFailure(t *testing.T) {
	cxt, mock := setUpHandlerContext("{\"to\":\"pqr\",\"from\":\"xyz\",\"text\":\"Hello\"}", &cache.RedisCacheMock{Success: false, Error: "Retrieval Error"})
	defer cxt.Env.DB.Close()

	cxt.Request.SetBasicAuth("xyz", "test")

	mock.ExpectQuery("SELECT id, auth_id FROM account WHERE .*").
		WithArgs("xyz").
		WillReturnRows(sqlmock.NewRows([]string{"id", "auth_id"}).AddRow(1, "test"))

	mock.ExpectQuery("SELECT id FROM phone_number WHERE .*").
		WithArgs(1, "xyz").
		WillReturnRows(sqlmock.NewRows([]string{"id"}).AddRow(1))

	output := HandleOutboundSMS(cxt)
	assert.NotNil(t, output)
	assert.Equal(t, "", output.Message)
	assert.Equal(t, "Retrieval Error", output.Error)
	assert.Equal(t, http.StatusInternalServerError, output.StatusCode)
}

func TestOutboundSMSCheckForSendSuccess(t *testing.T) {
	cxt, mock := setUpHandlerContext("{\"to\":\"pqr\",\"from\":\"xyz\",\"text\":\"Hello\"}", &cache.RedisCacheMock{Success: true, Error: ""})
	defer cxt.Env.DB.Close()

	cxt.Request.SetBasicAuth("xyz", "test")

	mock.ExpectQuery("SELECT id, auth_id FROM account WHERE .*").
		WithArgs("xyz").
		WillReturnRows(sqlmock.NewRows([]string{"id", "auth_id"}).AddRow(1, "test"))

	mock.ExpectQuery("SELECT id FROM phone_number WHERE .*").
		WithArgs(1, "xyz").
		WillReturnRows(sqlmock.NewRows([]string{"id"}).AddRow(1))

	output := HandleOutboundSMS(cxt)
	assert.NotNil(t, output)
	assert.Equal(t, "outbound sms ok", output.Message)
	assert.Equal(t, "", output.Error)
	assert.Equal(t, http.StatusOK, output.StatusCode)
}
