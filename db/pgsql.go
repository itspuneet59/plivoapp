package db

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
)

type PGSql struct {
	db *sql.DB
}

func connect(host string, port int, username string, password string, dbName string) (error, *sql.DB) {
	db, err := sql.Open("postgres", fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", host, port, username, password, dbName))
	if err != nil {
		return err, nil
	}

	return nil, db
}

func NewPGSql(host string, port int, username string, password string, dbName string) (error, *PGSql) {
	err, db := connect(host, port, username, password, dbName)
	if err != nil {
		return err, nil
	}

	return nil, &PGSql{
		db: db,
	}
}

func (pgsql *PGSql) QueryRow(query string, args ...interface{}) *sql.Row {
	return pgsql.db.QueryRow(query, args...)

}

func (pgsql *PGSql) Close() {
	pgsql.Close()
}
